﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PluginWrapper : MonoBehaviour {

	// Use this for initialization
	void Start () {
		TextMesh textMesh = GetComponent<TextMesh> ();
		var plugin = new AndroidJavaClass ("androidproject.alientechlab.com.unityplugin.PluginClass");
		textMesh.text = plugin.CallStatic<string> ("GetTextFromPlugin", 7);
	}

}
